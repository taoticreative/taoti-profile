core = "7.x"
api = "2"

projects[admin_menu][subdir] = "contrib"

projects[admin_views][subdir] = "contrib"

projects[addressfield][subdir] = "contrib"

projects[backup_migrate][subdir] = "contrib"

projects[botcha][subdir] = "contrib"

projects[ckeditor][subdir] = "contrib"

projects[coffee][subdir] = "development"

projects[compact_forms][subdir] = "contrib"

projects[content_access][subdir] = "contrib"

projects[context][subdir] = "contrib"

projects[ctools][subdir] = "contrib"

projects[date][subdir] = "contrib"

projects[devel][subdir] = "development"

projects[devel_themer][subdir] = "development"

projects[diff][subdir] = "contrib"

projects[email][subdir] = "contrib"

projects[entity][subdir] = "contrib"

projects[facetapi][subdir] = "contrib"

projects[features][subdir] = "contrib"

projects[field_group][subdir] = "contrib"

projects[globalredirect][subdir] = "contrib"

projects[google_analytics][subdir] = "contrib"

projects[imce][subdir] = "contrib"

projects[imce_wysiwyg][subdir] = "contrib"

projects[link][subdir] = "contrib"

projects[linkit][subdir] = "contrib"

projects[logintoboggan][subdir] = "contrib"

projects[mail_redirect][subdir] = "development"

projects[manualcrop][subdir] = "contrib"

projects[metatag][subdir] = "contrib"

projects[moopapi][subdir] = "contrib"

projects[module_filter][subdir] = "contrib"

projects[pathauto][subdir] = "contrib"

projects[rules][subdir] = "contrib"

projects[search_api][subdir] = "contrib"

projects[search_api_db][subdir] = "contrib"

projects[search_api_page][subdir] = "contrib"

projects[search_krumo][subdir] = "development"

projects[security_review][subdir] = "development"

projects[select_or_other][subdir] = "contrib"

projects[seotools][subdir] = "contrib"

projects[stage_file_proxy][subdir] = "development"

projects[strongarm][subdir] = "development"

projects[token][subdir] = "contrib"

projects[views][subdir] = "contrib"

projects[webform][subdir] = "contrib"

projects[weight][subdir] = "contrib"

projects[xmlsitemap][subdir] = "contrib"

projects[] = rubik

projects[] = shiny

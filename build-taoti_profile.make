api = 2
core = 7.x

includes[] = drupal-org-core.make
includes[] = drupal-org.make
includes[] = drupal-org-core.make

projects[taoti_profile][download][type] = "git"
projects[taoti_profile][download][url] = "git@bitbucket.org:taoticreative/taoti-profile.git"
projects[taoti_profile][type] = "profile"

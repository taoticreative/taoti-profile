<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function taoti_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/*
 * Implements hook_install_tasks_alter()
 */
function taoti_profile_install_tasks_alter(&$tasks, $install_state) {

  // substitute our own finished step
  $tasks['install_finished'] = array(
    'display_name' => st('Finished Installation'),
    'display' => 1,
    'function' => 'taoti_profile_install_finished',
  );

}

function taoti_profile_install_finished(&$install_state) {

  // Remember the profile which was used.
  variable_set('install_profile', drupal_get_profile());

  // Installation profiles are always loaded last
  db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('type', 'module')
    ->condition('name', drupal_get_profile())
    ->execute();

  //Revert Features components that get overridden
  features_revert(array('taoti_default_wysiwyg' => array('ckeditor_profile')));
  features_revert(array('taoti_faceted_search' => array('search_api_index')));

  // Flush all caches to ensure that any full bootstraps during the installer
  // do not leave stale cached data, and that any content types or other items
  // registered by the install profile are registered correctly.
  drupal_flush_all_caches();


  // Run cron to populate update status tables (if available) so that users
  // will be warned if they've installed an out of date Drupal version.
  // Will also trigger indexing of profile-supplied content or feeds.
  drupal_cron_run();



  if (isset($messages['error'])) {
    $output = '<p>' . (isset($messages['error']) ? st('Review the messages above before visiting <a href="@url">your new site</a>.', array('@url' => url(''))) : st('<a href="@url">Visit your new site</a>.', array('@url' => url('')))) . '</p>';
    return $output;
  }
  else {

    $output =  '<h1>Thank you for installing Taoti Profile.</h1>
                <p>A few notes about the profile:</p>
                <ul>
                    <li>Backup Migrate has been included in this profile. When starting a new project it is a good idea to configure a secure private directory as well as set a schedule.</li>
                    <li>Most of the features are now removed, excluding the WYSIWYG configuration. This is enabled with the default install. The pre-configured WYSIWYIG Feature utlizes CKeditor and IMCE as it\'s file manager. It comes as a configurable feature.</li>
                    <li>By default the Taoti: Faceted Search feature is turned on. This faceted search utilizes search api and does not need the default search module or apache solr to run. You can however, configure this faceted search to run off of solr.</li>
                </ul>';
    $output .= '<p>' . st('<a href="@url">Visit your new site</a>.', array('@url' => url(''))) . '</p>';
    return $output;

  }


} // taoti_profile_install_finished()
